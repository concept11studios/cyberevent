// React
import React from 'react'

// Material UI
import { Grid, Card, withStyles } from '@material-ui/core'

// Assets
import Concept11Logo from '../../assets/c11_logo.png'
import InventiveGuildLogo from '../../assets/ig_logo.png'
import CyberEventLogo from '../../assets/cyberevent_logo.png'

const GetLogoFromPath = (path) => {
    switch(path) {
        case 'concept11' :
            return Concept11Logo
        case 'inventiveguild' :
            return InventiveGuildLogo
        default :
            return CyberEventLogo
    }
}

const styles = (theme) => ({
    wrapper: {
        backgroundColor: '#1B2436',
        display: 'flex'
    },
    eventContainer: {
        flex: 1,
        padding: 0,
    },
    titleCard: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: theme.palette.primary.dark,
        padding: theme.spacing(2),
        color: '#FFFFFF',
        boxShadow: 'none',
        borderRadius: 0
    }
})

const Navbar = ({ classes, path }) => {
    return (
        <Grid
        xs = { 12 }
        container
        className = { classes.wrapper }
        >
            <Grid
            xs = { 12 }
            item
            className = { classes.eventContainer }
            >
                <Card
                className = { classes.titleCard }
                >
                    <img
                    width = { 100 }
                    src = { GetLogoFromPath(path) }
                    />
                </Card>
            </Grid>
        </Grid>
    )
}

export default withStyles(styles, { withTheme: true })(Navbar)
