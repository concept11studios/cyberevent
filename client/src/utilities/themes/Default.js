// Material UI
import { createMuiTheme } from '@material-ui/core/styles'
import red from '@material-ui/core/colors/red'
import grey from '@material-ui/core/colors/grey'

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#FFFFFF',
            lightMain: '#2F3B52',
            main: red[500],
            midDark: '#2F3B52',
            highDark: '#131A27',
            dark: '#242E42',
            contrast: '#FFFFFF',
            green: '#437B62',
            yellow: '#757B56',
            red: '#755156'
        },
        secondary: {
            main: red[500],
            dark: '#1B2436'
        }
    },
    borderRadius: 15
})

export default theme