// React
import React from 'react'

// Material UI
import { Grid, withStyles } from '@material-ui/core'

// Components
import ExpandedSchedule from './ExpandedSchedule'
import UnexpandedSchedule from './UnexpandedSchedule'

const styles = (theme) => ({
    scheduleContainer: {
        padding: theme.spacing(2)
    },
    scheduleCard: {
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden',
        maxHeight: `calc(100vh - ${ theme.spacing(4) }px)`,
        textAlign: 'left',
        backgroundColor: theme.palette.primary.dark,
        borderRadius: theme.borderRadius,
        boxShadow: 'none',
        color: theme.palette.primary.contrast
    },
    scheduleTitleContainer: {
        padding: theme.spacing(2)
    },
    scheduleTitle: {
        fontWeight: 600
    },
    viewAgendaText: {
        fontSize: 14,
        fontWeight: 600,
        color: '#9297A1'
    },
    breakoutTitleContainer: {
        
    },
    breakoutTitleActive: {
        backgroundColor: theme.palette.primary.dark,
        padding: theme.spacing(2),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    breakoutTitleInactive: {
        backgroundColor: theme.palette.primary.lightMain,
        padding: theme.spacing(2),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    breakoutTitle: {
        textAlign: 'center',
        fontWeight: 600
    },
    sessionsContainer: {
        overflowY: 'scroll',
        'scrollbar-width': 'thin',
        '&::-webkit-scrollbar': {
            width: 5,
            backgroundColor: theme.palette.secondary.dark
        },
        '&::-webkit-scrollbar-thumb': {
            backgroundColor: '#131A27',
            borderRadius: 15
        }
    },
    scheduleDateContainerActive: {
        backgroundColor: theme.palette.primary.highDark,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    scheduleDateContainerInactive: {
        backgroundColor: theme.palette.primary.midDark,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    scheduleDayText: {
        fontSize: 14,
        fontWeight: 600
    },
    scheduleDateText: {
        fontSize: 24,
        fontWeight: 500
    },
    greenSchedule: {
        backgroundColor: theme.palette.primary.green,
        border: '1px solid #77FD96',
        padding: theme.spacing(1),
        borderRadius: 5
    },
    yellowSchedule: {
        backgroundColor: theme.palette.primary.yellow,
        border: '1px solid #FDFD77',
        padding: theme.spacing(1),
        borderRadius: 5
    },
    redSchedule: {
        backgroundColor: theme.palette.primary.red,
        border: '1px solid #FD8C77',
        padding: theme.spacing(1),
        borderRadius: 5
    },
    expandIcon: {
        color: theme.palette.primary.contrast
    }
})

const breakouts = [
    {
        title: 'PAM Breakout',
        schedule: [
            {
                day: 'THU',
                date: '26',
                sessions: [
                    {
                        presentor: 'Istvan Jagrl',
                        title: 'How Safeguard supports Cloud',
                        time: '9 AM EST - 11 AM EST'
                    },
                    {
                        presentor: 'Daniel Conrad',
                        title: 'Just-in-time provisioning of Active Directory',
                        time: '11 AM EST - 2 PM EST'
                    },
                    {
                        presentor: 'Dan Peterson',
                        title: 'Frictionless DevOps Secrets Management in Safeguard',
                        time: '2 PM EST - 4 PM EST'
                    }
                ]
            },
            {
                day: 'FRI',
                date: '27',
                sessions: [
                    {
                        presentor: 'Todd Miller',
                        title: "What's new sudo 1.9",
                        time: '9 AM EST - 11 AM EST'
                    },
                    {
                        presentor: 'Alex Binotto',
                        title: 'One Identity Manager and Where we are Today',
                        time: '11 AM EST - 2 PM EST'
                    },
                    {
                        presentor: 'Alex Binotto',
                        title: 'OIGA Path to SaaS',
                        time: '2 PM EST - 4 PM EST'
                    }
                ]
            },
            {
                day: 'SAT',
                date: '28',
                sessions: [
                    {
                        presentor: 'Hanno Bunjes',
                        title: 'Time to Prove Applications are under Governance',
                        time: '9 AM EST - 11 AM EST'
                    }
                ]
            }
        ]
    },
    {
        title: 'IGA Breakout'
    },
    {
        title: 'AD Breakout'
    }
]

const Schedule = (props) => {
    const { classes, expandables, handleExpandables } = props

    return (
        <>
            { expandables.schedule ?
                <ExpandedSchedule
                classes = { classes }
                breakouts = { breakouts }
                handleExpandables = { handleExpandables }
                />
                :
                <UnexpandedSchedule
                classes = { classes }
                breakouts = { breakouts }
                handleExpandables = { handleExpandables }
                />
            }
        </>
    )
}

export default withStyles(styles, { withTheme: true })(Schedule)
