// Material UI
import { createMuiTheme } from '@material-ui/core/styles'
import grey from '@material-ui/core/colors/grey'

const theme = createMuiTheme({
    palette: {
        primary: {
            light: grey[900],
            lightMain: grey[200],
            main: '#2964f2',
            midDark: grey[300],
            highDark: grey[200],
            dark: '#FFFFFF',
            contrast: grey[900],
            green: '#c2fed0',
            yellow: '#fefec2',
            red: '#feccc2'
        },
        secondary: {
            main: '#2964f2',
            dark: grey[100]
        }
    },
    borderRadius: 0
})

export default theme