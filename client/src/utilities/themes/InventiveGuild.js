// Material UI
import { createMuiTheme } from '@material-ui/core/styles'
import grey from '@material-ui/core/colors/grey'

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#FFFFFF',
            lightMain: grey[800],
            main: '#BAA05E',
            midDark: grey[700],
            highDark: grey[800],
            dark: grey[900],
            contrast: '#FFFFFF',
            green: '#437B62',
            yellow: '#757B56',
            red: '#755156'
        },
        secondary: {
            main: '#BAA05E',
            dark: '#2E2E2E'
        }
    },
    borderRadius: 15
})

export default theme