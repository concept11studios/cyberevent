import React from 'react'

// Material UI
import { Grid, Card, Chip, Typography, withStyles, Avatar, Divider, Button, CardContent, CardMedia } from '@material-ui/core'
import AvatarGroup from '@material-ui/lab/AvatarGroup';

// Components
import Navbar from './Navbar'

const breakouts = [
    {
        title: 'Edge Computing',
        description: 'Edge computing is computing that’s done at or near the source of the data, instead of relying on the cloud at one of a dozen data centers to do all the work.',
        members: 46,
        online: 42
    },
    {
        title: 'Edge Computing',
        description: 'Edge computing is computing that’s done at or near the source of the data, instead of relying on the cloud at one of a dozen data centers to do all the work.',
        members: 46,
        online: 42
    }
]

const membersArray = Array.apply(null, Array(46)).map(function () {})

const styles = (theme) => ({
    wrapper: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
        backgroundColor: theme.palette.secondary.dark,
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    typography: {
        fontWeight: 500
    },
    mainEventButton: {
        paddingRight: 24,
        paddingLeft: 24,
        color: theme.palette.primary.light,
        background: theme.palette.secondary.dark
    },
    breakoutText: {
        color: 'lightgrey'
    },
    breakoutTitle: {
        display: 'flex',
        marginTop: 24
    },
    breakoutCard: {
        backgroundColor: theme.palette.primary.dark,
        borderRadius: theme.borderRadius,
        display: 'flex',
        flexDirection: 'column',
        margin: 'unset',
        marginBottom: 8,
        padding: theme.spacing(2),
        height: 300,
        color: theme.palette.primary.light,
        boxShadow: 'none'
    },
    mainEventCard: {
        marginTop: theme.spacing(1),
        backgroundColor: theme.palette.primary.dark,
        borderRadius: theme.borderRadius,
        display: 'flex',
        flexDirection: 'column',
        margin: 'unset',
        marginBottom: 8,
        padding: theme.spacing(2),
        height: 350,
        color: theme.palette.primary.light,
        boxShadow: 'none' 
    },
    joinButton: {
        marginTop: 'auto',
        background: theme.palette.secondary.dark,
        color: theme.palette.primary.light
    },

    cardMedia: {
        width: 180, height: 180
    }
})

class Landing extends React.Component {
    render() {
        const { classes, path, setActiveSession } = this.props

        return (
            <Grid
            container
            xs = { 12 }
            >
                <Navbar
                path = { path }
                />
                <Grid
                className = { classes.wrapper }
                container
                xs = { 7 }
                >
                    <Grid
                    xs = { 8 }
                    item
                    className = { classes.eventContainer }
                    >
                        <Card
                        className = { classes.titleCard }
                        >
                            <Grid
                            xs = { 12 }
                            container
                            >
                                <div
                                style = {{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', flex: 1 }}
                                >
                                    <div
                                    style = {{ display: 'flex' }}
                                    >
                                        <Chip
                                        size = 'small'
                                        label = 'LIVE'
                                        className = { classes.liveChip }
                                        />
                                        <Typography
                                        className = { classes.eventTitle }
                                        >
                                            How Safeguard Supports Cloud
                                        </Typography>
                                    </div>
                                    <Button
                                    className = { classes.mainEventButton }
                                    onClick = { () => setActiveSession(true) }
                                    >
                                        Join Main Event
                                    </Button>
                                </div>
                            </Grid>
                        </Card>
                        <div
                        style = { { padding: '56.25% 0 0 0', position: 'relative' } }
                        >
                            <iframe
                            src = "https://vimeo.com/event/679961/embed"
                            frameborder = "0"
                            allow = "autoplay; fullscreen"
                            allowfullscreen
                            className = { classes.video }
                            >
                                
                            </iframe>
                        </div>
                        <Card
                        style = {{ position: 'relative' }}
                        className = { classes.mainEventCard }
                        >
                            <CardContent
                            style = {{ paddingTop: 0 }}
                            >
                                <Chip
                                size = 'small'
                                label = 'LIVE'
                                className = { classes.liveChip }
                                style = {{ position: 'absolute', top: 24, right: 10 }}
                                />
                                <Typography
                                style = {{ display: 'flex' }}
                                variant = 'h3'
                                >
                                    Main Event
                                </Typography>
                                <Typography
                                variant = 'h6'
                                style = {{ display: 'flex' }}
                                >
                                    How Safeguard Supports Cloud
                                </Typography>
                                <div
                                style = {{ display: 'flex', marginTop: 24 }}
                                >
                                    <Card
                                    style = {{ background: 'transparent', color: '#fff', boxShadow: 'unset', marginRight: 24, width: 180 }}
                                    >
                                        <CardMedia
                                        className = { classes.cardMedia }
                                        image = 'https://material-ui.com/static/images/avatar/2.jpg'
                                        />
                                        <CardContent
                                        style = {{ display: 'flex', paddingLeft: 0, paddingRight: 0, textAlign: 'start' }}
                                        >
                                            Privacy and Cloud Computing
                                        </CardContent>
                                    </Card>
                                    <Card
                                    style = {{ background: 'transparent', color: '#fff', boxShadow: 'unset', marginRight: 24, width: 180 }}
                                    >
                                        <CardMedia
                                        className = { classes.cardMedia }
                                        image = 'https://material-ui.com/static/images/avatar/4.jpg'
                                        />
                                        <CardContent
                                        style = {{ display: 'flex', paddingLeft: 0, paddingRight: 0, textAlign: 'start' }}
                                        >
                                            The Future of Edge Computing
                                        </CardContent>
                                    </Card>
                                    <Card
                                    style = {{ background: 'transparent', color: '#fff', boxShadow: 'unset', marginRight: 24, width: 180 }}
                                    >
                                        <CardMedia
                                        className = { classes.cardMedia }
                                        image = 'https://material-ui.com/static/images/avatar/5.jpg'
                                        />
                                        <CardContent
                                        style = {{ display: 'flex', paddingLeft: 0, paddingRight: 0, textAlign: 'start' }}
                                        >
                                            The Future of IOT
                                        </CardContent>
                                    </Card>
                                </div>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Grid
                className = { classes.wrapper }
                container
                xs = { 5 }
                >
                    <Grid
                    xs = { 8 }
                    item
                    className = { classes.eventContainer }
                    >
                        <Grid
                        xs = { 12 }
                        container
                        >
                            <Typography
                            variant = 'h6'
                            className = { classes.eventTitle }
                            gutterBottom
                            >
                                Breakouts
                            </Typography>
                        </Grid>
                        { breakouts.map(breakout => 
                            <Card
                            className = { classes.breakoutCard }
                            >
                                <AvatarGroup max={10}>
                                    { membersArray.map((member, index) => {
                                        return <Avatar alt="Remy Sharp" src={ `https://material-ui.com/static/images/avatar/${index+1}.jpg` } />
                                    }) }
                                </AvatarGroup>
                                <Typography
                                variant = 'h6'
                                className = { classes.breakoutTitle }
                                >
                                    { breakout.title }
                                </Typography>
                                <Typography
                                className = { classes.breakoutText }
                                style = {{ display: 'flex', marginTop: 8, textAlign: 'start' }}
                                >
                                    { breakout.description }
                                </Typography>
                                <div 
                                style = {{ display: 'flex', marginTop: 12, alignItems: 'center' }}
                                >
                                    <Typography
                                    style = {{ display: 'flex' }}
                                    >
                                        { breakout.members } members
                                    </Typography>
                                    <Divider
                                    style = {{ marginRight: 12, marginLeft: 12 }}
                                    orientation = 'vertical'
                                    />
                                    <div
                                    style = {{ background: 'green', width: 8, height: 8, borderRadius: '50%', marginRight: 8 }}
                                    />
                                    <Typography
                                    style = {{ display: 'flex' }}
                                    >
                                        { breakout.online } online
                                    </Typography>
                                </div>
                                <Button
                                className = { classes.joinButton } 
                                onClick = { () => setActiveSession(true) }
                                >
                                    Join Breakout
                                </Button>
                            </Card>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles, { withTheme: true })(Landing)


