"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Express & Middleware
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const http_1 = __importDefault(require("http"));
// Express
const app = express_1.default();
const router = express_1.default.Router();
const server = http_1.default.createServer(app);
app.use(router);
app.use(express_1.default.static(path_1.default.join(__dirname, '/client/build')));
const port = 5000;
// var accessToken = '02C1QymiJknWRkqWBLI8DkMKncsal9yCWs07ExLxJBZi7KAx_tV7o0ql1N-idccqKqwz47yULbNv-HV-g_Rk6bCKxqaJ0'
// var client = new RevAiApiClient(accessToken)
// var audioConfig = new AudioConfig() // Initialize audio configuration for the streaming client
// var streamingClient = new RevAiStreamingClient(accessToken, audioConfig)
// streamingClient.on('close', (code, reason) => {
//     console.log(`Connection closed, ${code}: ${reason}`)
// });
// streamingClient.on('connect', connectionMessage => {
//     console.log(`Connected with job id: ${connectionMessage.id}`)
// })
// let vimeoClient = new Vimeo("d806a75c0eb33d7f4188bd2947942557103094bd", "MYVz2ZvfCdeK0OyOmzDGV0XRJisK/6j0ABJlE8rLJ2oS1RrZnznsBid7Etok8OFED3DKF7X3O5yhRveKwvNI8pXlOt/vh9jdhWaDqwgwrDUSA6MfW2PpfuZUA8EYWyw5", "a87f9cf47600d94a63631b5bf4536c7d")
// vimeoClient.request({
//     method: 'GET',
//     path: 'https://api.vimeo.com/me/videos/492913573/m3u8_playback'
// }, (error, body, status_code, headers) => {
//     if (error) {
//         console.log('error');
//         console.log(error);
//       } else {
//         console.log('body');
//         console.log(body);
//       }
//       console.log('status code');
//       console.log(status_code);
//       console.log('headers');
//       console.log(headers);
// })
// console.log(vimeoClient)
// // Begin streaming session
// var stream = streamingClient.start()
// stream.on('data', data => {
//     console.log(data)
// })
// stream.on('end', function () {
//     console.log("End of Stream")
// })
// Server without HTTPS
server.listen(port, () => {
    return console.log(`I am listening on ${port}, and env is \n ${process.env.NODE_ENV}`);
}).on('error', (e) => {
    console.log(e);
});
function asyncForEach(array, callback) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let index = 0; index < array.length; index++) {
            yield callback(array[index], index, array);
        }
    });
}
router.post('/api/test-bidcore', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
}));
// Default Routing
app.get('/*', function (req, res) {
    res.sendFile(path_1.default.join(__dirname, 'client/build/index.html'), function (err) {
        if (err) {
            res.status(500).send(err);
        }
    });
});
