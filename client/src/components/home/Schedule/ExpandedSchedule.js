// React
import React from 'react'

// Material UI
import { Card, Grid, IconButton, Typography } from '@material-ui/core'
import MenuOpenIcon from '@material-ui/icons/MenuOpen'

const ExpandedSchedule = (props) => {
    const { classes, breakouts, handleExpandables } = props

    return (
        <Grid
        xs = { 3 }
        item
        className = { classes.scheduleContainer }
        >
            <Card
            className = { classes.scheduleCard }
            >
                <Grid
                xs = { 12 }
                container
                justify = 'space-between'
                alignItems = 'center'
                className = { classes.scheduleTitleContainer }
                >
                    <div
                    style = { { display: 'flex', flexDirection: 'row' } }
                    >
                        <IconButton
                        style = { { padding: 0, marginRight: 16 } }
                        >
                            <MenuOpenIcon
                            className = { classes.expandIcon }
                            onClick = { handleExpandables }
                            />
                        </IconButton>
                        <Typography
                        className = { classes.scheduleTitle }
                        >
                            Schedule
                        </Typography>
                    </div>
                    <div>
                        <Typography
                        className = { classes.viewAgendaText }
                        >
                            View full agenda
                        </Typography>
                    </div>
                </Grid>
                <Grid
                xs = { 12 }
                container
                className = { classes.breakoutTitleContainer }
                >
                    { breakouts.map((breakout, index) => (
                        <Grid
                        xs = { 4 }
                        item
                        className = { index === 0 ? classes.breakoutTitleActive : classes.breakoutTitleInactive }
                        >
                            <Typography
                            className = { classes.breakoutTitle }
                            >
                                { breakout.title }
                            </Typography>
                        </Grid>
                    )) }
                </Grid>
                <Grid
                xs = { 12 }
                container
                className = { classes.sessionsContainer }
                >
                    { breakouts[0].schedule.map((schedule, index) => (
                        <Grid
                        xs = { 12 }
                        container
                        >
                            <Grid
                            xs = { 3 }
                            item
                            className = { index === 0 ? classes.scheduleDateContainerActive : classes.scheduleDateContainerInactive }
                            >
                                <Typography
                                className = { classes.scheduleDayText }
                                >
                                    { schedule.day }
                                </Typography>
                                <Typography
                                className = { classes.scheduleDateText }
                                >
                                    { schedule.date }
                                </Typography>
                            </Grid>
                            <Grid
                            xs = { 9 }
                            item
                            container
                            >
                                { schedule.sessions.map(session => (
                                    <Grid
                                    xs = { 12 }
                                    className = { index === 0 ? classes.greenSchedule : index === 1 ? classes.yellowSchedule : classes.redSchedule }
                                    >
                                        <Typography>
                                            { session.presentor }
                                        </Typography>
                                        <Typography>
                                            { session.title }
                                        </Typography>
                                        <Typography>
                                            { session.time }
                                        </Typography>
                                    </Grid>
                                )) }
                            </Grid>
                        </Grid>
                    )) }
                </Grid>
            </Card>
        </Grid>
    )
}

export default ExpandedSchedule
