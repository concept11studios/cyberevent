// React
import React from 'react'

// Material UI
import { Grid, Card, IconButton, Typography } from '@material-ui/core'
import MenuOpenIcon from '@material-ui/icons/MenuOpen'

const ExpandedRightMenu = (props) => {
    const { classes, presentors, handleExpandables } = props

    return (
        <Grid
        xs = { 3 }
        item
        className = { classes.presentorsContainer }
        >
            <Card
            className = { classes.presentorsCard }
            style = { { marginBottom: 20 } }
            >
                <Grid
                xs = { 12 }
                container
                justify = 'space-between'
                alignItems = 'center'
                className = { classes.scheduleTitleContainer }
                >
                    <div
                    style = { { display: 'flex', flexDirection: 'row' } }
                    >
                        <IconButton
                        style = { { padding: 0, marginRight: 16, transform: 'scaleX(-1)' } }
                        >
                            <MenuOpenIcon
                            className = { classes.expandIcon }
                            onClick = { handleExpandables }
                            />
                        </IconButton>
                        <Typography
                        className = { classes.presentorTitle }
                        >
                            Presentors
                        </Typography>
                    </div>
                </Grid>
                { presentors.map(presentor => (
                    <Grid
                    xs = { 12 }
                    container
                    className = { classes.singlePresentorContainer }
                    >
                        <img
                        height = '50'
                        width = '50'
                        src = { presentor.photo }
                        className = { classes.presentorPhoto }
                        />
                        <Grid
                        item
                        >
                            <Typography
                            className = { classes.presentorName }
                            >
                                { presentor.name }
                            </Typography>
                            <Typography>
                                { presentor.title }
                            </Typography>
                        </Grid>
                    </Grid>
                )) }
            </Card>
            <Card>
            <iframe src="https://app.sli.do/event/uupqr7ni" height="100%" width="100%" frameBorder="0" style= { { minHeight: 560 } } ></iframe>
            {/* <iframe src="https://vimeo.com/event/604362/chat/" width="100%" height="100%" frameborder="0"  style = { { minHeight: 500 } } s></iframe> */}
            </Card>
        </Grid>
    )
}

export default ExpandedRightMenu
