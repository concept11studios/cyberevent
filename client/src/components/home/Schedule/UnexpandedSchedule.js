// React
import React from 'react'

// Material UI
import { Card, Grid, IconButton, Typography } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'

const UnexpandedSchedule = (props) => {
    const { classes, handleExpandables } = props

    return (
        <Card
        className = { classes.scheduleCard }
        style = { { margin: 16 } }
        >
            <Grid
            xs = { 12 }
            container
            alignItems = 'flex-start'
            className = { classes.scheduleTitleContainer }
            >
                <IconButton
                style = { { padding: 0 } }
                >
                    <MenuIcon
                    className = { classes.expandIcon }
                    onClick = { handleExpandables }
                    />
                </IconButton>
            </Grid>
        </Card>
    )
}

export default UnexpandedSchedule
