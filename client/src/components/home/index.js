// React
import React from 'react'

// Material UI
import { Grid, Card, Chip, Typography, withStyles } from '@material-ui/core'
import PersonIcon from '@material-ui/icons/Person'

// Components
import Navbar from './Navbar'
import Schedule from './Schedule/index'
import RightMenu from './RightMenu/index'
import Landing from './Landing'

const styles = (theme) => ({
    wrapper: {
        backgroundColor: theme.palette.secondary.dark,
        minHeight: '100vh',
        display: 'flex',
    },
    eventContainer: {
        flex: 1,
        paddingTop: theme.spacing(2),
        maxWidth: 'unset !important'
    },
    titleCard: {
        backgroundColor: theme.palette.primary.dark,
        borderRadius: theme.borderRadius,
        marginBottom: theme.spacing(2),
        padding: theme.spacing(2),
        color: theme.palette.primary.light,
        boxShadow: 'none'
    },
    liveChip: {
        backgroundColor: theme.palette.primary.main,
        color: '#FFFFFF',
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
        marginRight: theme.spacing(2),
        fontWeight: 700
    },
    eventTitle: {
        fontWeight: 500,
        color: theme.palette.primary.light,
    },
    userIcon: {
        color: '#00AEFC',
        marginRight: theme.spacing(1)
    },
    userCount: {
        color: '#00AEFC',
        fontWeight: 500
    },
    video: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: theme.borderRadius
    }    
})

const initialState = {
    expandables: {
        schedule: false,
        rightMenu: false
    },
    activeSession: false
}

class Home extends React.Component {
    state = initialState

    handleExpandables = (expandable) => {
        this.setState(prevState => ({ expandables: { ...prevState.expandables, [expandable]: !prevState.expandables[expandable] } }))
    }

    render() {
        const { classes, path } = this.props
        const { expandables, activeSession } = this.state

        if (activeSession) {
            return (
                <Grid
                container
                xs = { 12 }
                >
                    <Navbar
                    path = { path }
                    />
                    <Grid
                    className = { classes.wrapper }
                    container
                    xs = { 12 }
                    >
                        <Schedule
                        expandables = { expandables }
                        handleExpandables = { () => this.handleExpandables('schedule') }
                        />
                        <Grid
                        xs = { 6 }
                        item
                        className = { classes.eventContainer }
                        >
                            <Card
                            className = { classes.titleCard }
                            >
                                <Grid
                                xs = { 12 }
                                container
                                justify = 'space-between'
                                >
                                    <Grid
                                    xs = { 10 }
                                    item
                                    container
                                    alignItems = 'center'
                                    >
                                        <Chip
                                        size = 'small'
                                        label = 'LIVE'
                                        className = { classes.liveChip }
                                        />
                                        <Typography
                                        className = { classes.eventTitle }
                                        >
                                            How Safeguard Supports Cloud
                                        </Typography>
                                    </Grid>
                                    {/* <Grid
                                    xs = { 2 }
                                    item
                                    container
                                    alignItems = 'center'
                                    justify = 'flex-end'
                                    >
                                        <PersonIcon
                                        className = { classes.userIcon }
                                        />
                                        <Typography
                                        className = { classes.userCount }
                                        >
                                            3,462
                                        </Typography>
                                    </Grid> */}
                                </Grid>
                            </Card>
                            <div
                            style = { { padding: '56.25% 0 0 0', position: 'relative' } }
                            >
                                <iframe
                                src = "https://vimeo.com/event/679961/embed"
                                frameborder = "0"
                                allow = "autoplay; fullscreen"
                                allowfullscreen
                                className = { classes.video }
                                >
                                    
                                </iframe>
                            </div>
                        </Grid>
                        <RightMenu
                        expandables = { expandables }
                        handleExpandables = { () => this.handleExpandables('rightMenu') }
                        />
                    </Grid>
                </Grid>
            )    
        } else {
            return (
                <Landing 
                path = { path } 
                classes = { classes }
                setActiveSession = { (val) => this.setState({ activeSession: val }) }
                />
            )
        }
    }
}

export default withStyles(styles, { withTheme: true })(Home)
