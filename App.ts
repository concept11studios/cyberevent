// Express & Middleware
import express from 'express'
import path from 'path'
import http from 'http'
import https from 'https'

// Utilities
import _ from 'underscore'
import fs from 'fs'
import { Vimeo } from 'vimeo'
import { RevAiApiClient } from 'revai-node-sdk'
import { RevAiStreamingClient, AudioConfig } from 'revai-node-sdk'
import NodeMediaServer from 'node-media-server'

// Express
const app = express()
const router = express.Router()
const server = http.createServer(app)
app.use(router)
app.use(express.static(path.join(__dirname, '/client/build')))
const port = 5000

// var accessToken = '02C1QymiJknWRkqWBLI8DkMKncsal9yCWs07ExLxJBZi7KAx_tV7o0ql1N-idccqKqwz47yULbNv-HV-g_Rk6bCKxqaJ0'
// var client = new RevAiApiClient(accessToken)
// var audioConfig = new AudioConfig() // Initialize audio configuration for the streaming client
// var streamingClient = new RevAiStreamingClient(accessToken, audioConfig)

// streamingClient.on('close', (code, reason) => {
//     console.log(`Connection closed, ${code}: ${reason}`)
// });
 
// streamingClient.on('connect', connectionMessage => {
//     console.log(`Connected with job id: ${connectionMessage.id}`)
// })

// let vimeoClient = new Vimeo("d806a75c0eb33d7f4188bd2947942557103094bd", "MYVz2ZvfCdeK0OyOmzDGV0XRJisK/6j0ABJlE8rLJ2oS1RrZnznsBid7Etok8OFED3DKF7X3O5yhRveKwvNI8pXlOt/vh9jdhWaDqwgwrDUSA6MfW2PpfuZUA8EYWyw5", "a87f9cf47600d94a63631b5bf4536c7d")
// vimeoClient.request({
//     method: 'GET',
//     path: 'https://api.vimeo.com/me/videos/492913573/m3u8_playback'
// }, (error, body, status_code, headers) => {
//     if (error) {
//         console.log('error');
//         console.log(error);
//       } else {
//         console.log('body');
//         console.log(body);
//       }
    
//       console.log('status code');
//       console.log(status_code);
//       console.log('headers');
//       console.log(headers);
// })

// console.log(vimeoClient)

// // Begin streaming session
// var stream = streamingClient.start()

// stream.on('data', data => {
//     console.log(data)
// })

// stream.on('end', function () {
//     console.log("End of Stream")
// })

// Server without HTTPS
server.listen(port, () => {
    return console.log(`I am listening on ${port}, and env is \n ${process.env.NODE_ENV}`)
}).on('error', (e) => {
    console.log(e);
})

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

router.post('/api/test-bidcore', async (req, res) => {

})

// Default Routing
app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'client/build/index.html'), function (err) {
        if (err) {
            res.status(500).send(err)
        }
    })
})