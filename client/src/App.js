// Material UI
import { ThemeProvider } from '@material-ui/core/styles'

// Components
import Home from './components/home/index'

// Utilities
import './App.css'
import DefaultTheme from './utilities/themes/Default'
import Concept11Theme from './utilities/themes/Concept11Theme'
import InventiveGuildTheme from './utilities/themes/InventiveGuild'

const GetThemeFromPath = (path) => {
    switch(path) {
        case 'concept11' :
            return Concept11Theme
        case 'inventiveguild' :
            return InventiveGuildTheme
        default :
            return DefaultTheme
    }
}

function App() {
    const pathArray = window.location.pathname.split('/')

    return (
        <ThemeProvider
        theme = { GetThemeFromPath(pathArray[1]) }
        >
            <div
            className = "App"
            >
                <Home
                path = { pathArray[1] }
                />
            </div>
        </ThemeProvider>
    )
}

export default App;
