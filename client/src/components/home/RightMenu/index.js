// React
import React from 'react'

// Material UI
import { withStyles } from '@material-ui/core'

// Components
import ExpandedRightMenu from './ExpandedRightMenu'
import UnexpandedRightMenu from './UnexpandedRightMenu'

const styles = (theme) => ({
    presentorsContainer: {
        padding: theme.spacing(2)
    },
    presentorsCard: {
        textAlign: 'left',
        backgroundColor: theme.palette.primary.dark,
        borderRadius: theme.borderRadius,
        boxShadow: 'none',
        color: theme.palette.primary.contrast,
        padding: theme.spacing(2)
    },
    presentorTitle: {
        fontWeight: 600
    },
    presentorPhoto: {
        borderRadius: '100%',
        marginRight: theme.spacing(2)
    },
    presentorName: {
        fontWeight: 600
    },
    singlePresentorContainer: {
        marginTop: theme.spacing(2)
    },
    expandIcon: {
        color: theme.palette.primary.contrast
    }
})

const presentors = [
    {
        name: 'Istvan Jagrl',
        title: 'Safeguard Product Owner',
        photo: 'https://uifaces.co/our-content/donated/gPZwCbdS.jpg'
    },
    {
        name: 'Daniel Conrad',
        title: 'Technical Strategist',
        photo: 'https://uifaces.co/our-content/donated/FJkauyEa.jpg'
    }
]

const Presentors = (props) => {
    const { classes, expandables, handleExpandables } = props

    return (
        <>
            { expandables.rightMenu ?
                <ExpandedRightMenu
                classes = { classes }
                presentors = { presentors }
                handleExpandables = { handleExpandables }
                />
                :
                <UnexpandedRightMenu
                classes = { classes }
                handleExpandables = { handleExpandables }
                />
            }
        </>
    )
}

export default withStyles(styles, { withTheme: true })(Presentors)
